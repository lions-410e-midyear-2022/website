---
title: "Lions District 410E 2022 Midyear Conference"
date: 2019-07-14T16:19:07+02:00
draft: false
---

Welcome to the website of the 2022 [Lions District 410E](https://www.lions410e.org.za) Conference to be held 28 to 30 October at the Golden Gate Hotel, Clarens.

<!-- Click [here](/registration) to register for the convention. -->

<!-- Click [here](/news/newsflash_01) to view the latest convention newsletter. -->
