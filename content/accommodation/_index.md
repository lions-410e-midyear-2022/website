---
title: "Lions District 410E 2022 Midyear Conference Accommodation"
draft: false
---

Accommodation bookings must be made directly with Golden Gate National Park Hotel & Chalets.  Bookings open 26 July 2022 on a first come first served basis.  Call 058 255 1000 and quote reference no. 45793.
